-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2017 at 07:56 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `abbakka.tv`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mster_user`
--

CREATE TABLE IF NOT EXISTS `mster_user` (
  `user_id` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `mster_user`
--

INSERT INTO `mster_user` (`user_id`, `username`, `password`, `email`, `create_date`, `status`) VALUES
(1, 'bhavith', 'bhavith', 'hvsgvd@h.com', '2017-03-22', 1),
(2, 'admin', '$2y$10$/dOPcfphJ4pCvVZ6timZouV6yTUNz0RcNUFOW3XEkEoK9bsKld1kq', 'bhavithraj@gmail.com', '2017-03-23', 1),
(3, 'rohan', '$2y$10$og2p8wK6udjbdQEKxYvx6.6XGQQuxX32uTQmQO1JmH.vEEkveDnX.', 'bhav@gmail.com', '2017-03-23', 1),
(4, 'mohan', '$2y$10$p18MNNAIv5fytVFhwX2ScOeIemq88d1Nra6vpPBr7t/XFdEG/86vW', 'bsuds@lol.com', '2017-03-23', 1),
(5, 'bhavii', '$2y$10$TdzHfr49.BpNU18F.0zP/OlrpEQXug5R6xg3AgU45.SdmwRoc7CRq', 'bydfebf@j.com', '2017-03-23', 1),
(6, 'bhavithandel', '$2y$10$CEMJi4MjKptX22Nrnw7A6.8bPJwL0UaWWvhVcx4EwrvK/lBVDrQKG', 'abc@f.com', '2017-03-27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `scroll_news`
--

CREATE TABLE IF NOT EXISTS `scroll_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `scroll_news`
--

INSERT INTO `scroll_news` (`id`, `upd_date`, `content`) VALUES
(2, '2016-09-12 07:07:20', 'fbgdg'),
(3, '2017-03-06 09:33:34', 'vdfvdfvfdsvdsfvbfsvf');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
