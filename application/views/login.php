<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="ch-container">
    <div class="row">
        
    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>Welcome to ABBAKKA TV</h2>
				<?php if (validation_errors()) : ?>
				<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
			<?php endif; ?>
		
		        </div>
        <!--/span-->
    </div><!--/row-->
<?php if (isset($error)) : ?>
    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-info">
               <?= $error ?>
            </div>
			<?php endif; ?>
			<?= form_open() ?>
         <div class="form-horizontal" >
                <fieldset>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                        <input type="text" class="form-control"  id="username" autocomplete="off" required name="username" placeholder="Username">
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                        <input type="password" class="form-control" required id="password" name="password" placeholder="Password">
                    </div>
                    <div class="clearfix"></div>
					
					


                    <div class="input-prepend">
                        <label class="remember" for="remember"><input type="checkbox" id="remember"> Remember me</label>
                    </div>
                    <div class="clearfix"></div>

                    <p class="center col-md-5">
                         <input type="submit" class="btn btn-default" value="Login">   </p>
                </fieldset>
				</div>
            </form>
        </div>
        <!--/span-->
    </div><!--/row-->
</div><!--/fluid-row-->

</div><!--/.fluid-container-->
