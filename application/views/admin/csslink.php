
<title>Free </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="">

 <link id="bs-css" href="<?= base_url();?>admin_style/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?= base_url();?>admin_style/css/charisma-app.css" rel="stylesheet">
    <link href='<?= base_url();?>admin_style/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='<?= base_url();?>admin_style/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<?= base_url();?>admin_style/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<?= base_url();?>admin_style/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<?= base_url();?>admin_style/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<?= base_url();?>admin_style/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<?= base_url();?>admin_style/css/jquery.noty.css' rel='stylesheet'>
    <link href='<?= base_url();?>admin_style/css/noty_theme_default.css' rel='stylesheet'>
    <link href='<?= base_url();?>admin_style/css/elfinder.min.css' rel='stylesheet'>
    <link href='<?= base_url();?>admin_style/css/elfinder.theme.css' rel='stylesheet'>
    <link href='<?= base_url();?>admin_style/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<?= base_url();?>admin_style/css/uploadify.css' rel='stylesheet'>
    <link href='<?= base_url();?>admin_style/css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="<?= base_url();?>admin_style/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?= base_url();?>admin_style/img/favicon.ico">