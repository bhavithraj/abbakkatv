<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_model extends CI_Model  {



  public function construct()   
    
	{
	 parent::_construct();
	 $this->load->database();
	
	}
	
	
	  public function create_user($username, $password, $email)  
	     {
	        	$data= array(
			'username'   =>  $username,
			'password'   => $this->hash_password($password),
			'email'      => $email,
			'create_date' => date('Y-m-j H:i:s'),
			  'status' =>1);
			
		return $this->db->insert('mster_user', $data);
			 
		}
		
		
		
		public function user_login($username, $password)
		      {
			  $this->db->select('password');
			  $this->db->from('mster_user');
			  $this->db->where('username',$username );
					  
			  $hash=$this->db->get()->row('password');
			  
			  return $this->verify_password_hash($password, $hash);
			  
			  }
			  
			  private function hash_password($password) {
		
		         return password_hash($password, PASSWORD_BCRYPT);
		
	         }
	    public function verify_password_hash($password, $hash)
		    {
			
			 return password_verify($password, $hash);
			  			  
			}
			public function get_user_id_from_username($username)
			   {
			     $this->db->select('user_id');
				 $this->db->from('mster_user');
				 $this->db->where('username', $username);
				 return $this->db->get()->row('user_id');
				 
			  }
			 public function get_user($user_id)
			   {
			    $this->db->from('mster_user');
				$this->db->where('user_id', $user_id);
				return $this->db->get()->row();
				
			   } 
}