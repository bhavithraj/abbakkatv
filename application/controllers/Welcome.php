<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function _construct()
	  {
		  parent::__construct();
		  
		 	$this->load->library(array('session'));
	        $this->load->helper('url');
	     
	  }
	  
	  public function index()
	    {
		    $this->load->view('admin/csslink');
		    $this->load->view('login');
		}
	  
	  
	  public function register() {
		
		// create the data object
		$data = new stdClass();
		
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		   $this->load->model('user_model');
		// set validation rules
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|is_unique[mster_user.username]', array(  'is_unique' => 'This username already exists. Please choose another one.'));
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[mster_user.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');
		
		if ($this->form_validation->run() === false) {
			
			// validation not ok, send validation errors to the view
			
			$this->load->view('register/register', $data);
			
			
		} else {
			
			// set variables from the form
			$username = mysql_real_escape_string($_POST['username']);
			$password = mysql_real_escape_string($_POST['password']);
			$email    = mysql_real_escape_string($_POST['email']);
		
			
			if ($this->user_model->create_user($username, $password, $email)) {
				
				$data->error='User Created Secussfully';
				$this->load->view('register/register', $data);
				
			} else {
				
				// user creation failed, this should never happen
				$data->error = 'There was a problem creating your new account. Please try again.';
				
				// send error to the view
				
				$this->load->view('register/register', $data);
			
		
				
			}
			
		}
		
	}
	  public function login()
	       {
		     $data = new stdClass();
		
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
echo "".$this->form_validation->run();
		   $this->load->model('user_model');
			$this->form_validation->set_rules('username','username','required|alpha_numeric');
			 $this->form_validation->set_rules('password','password','required');
			
			   if($this->form_validation->run() == false)
		       	{
				       $data->error='please enter the username and password';
					   $this->load->view('admin/csslink');
					  $this->load->view('login', $data);
					
		        }
			  else
			    {
			      $username= mysql_real_escape_string($_POST['username']);
				  $password=mysql_real_escape_string($_POST['password']);
					
					 if($this->user_model->user_login($username, $password))
					    {
						$user_id=$this->user_model->get_user_id_from_username($username);
						$user=$this->user_model->get_user($user_id);
						//sesssion assign
						$_SESSION['user_id']=(int)$user_id;
						$_SESSION['username']=(string)$username;
						$_SESSION['logged_in']=(bool)true;
						$_SESSION['status']=(bool)'1';
						  $this->load->view('admin/csslink');
					    $this->load->view('admin/ad_header', $data);
							redirect('dashbord', 'refresh');
											
						}
						
					else
					   {
					   $data->error='wrong username or password.';
					    $this->load->view('admin/csslink');
					  $this->load->view('login', $data);
					  $this->load->view('admin/jslink');
					   }
					
								
					
		         }
	  
		   }
		
	
}
